var os      = require('os');
var http    = require('http');
var util    = require('util');

var port    = process.env.PORT || 8080;
var ip      = process.env.IP || '0.0.0.0';
var version = process.env.VERSION || '1.0.0';
var hostname= os.hostname();
var inet    = os.networkInterfaces()
var msg     = util.format("Hostname: %s", hostname)

http.createServer(function (req, res) {

    res.writeHead(200, {'Content-Type': 'text/plain'});

    res.end(util.format(
        "%s\n%s %s\n%s",
        msg,
        req.method,
        req.url,
        Object.getOwnPropertyNames(req.headers).map(function(k) { return k + ":" + req.headers[k]}).join("\n")
    ))

}).listen(port, ip);

console.log('Server running on %s at http://%s:%d/', hostname, ip, port);
