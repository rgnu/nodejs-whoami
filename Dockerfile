FROM quay.io/rgnu/alpine-node:0.10

WORKDIR /data

ENV PORT=8080

COPY . /data

EXPOSE $PORT

USER nobody

CMD ["/usr/bin/node", "server.js"]